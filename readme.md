# Reddit DBMS in SQL(PostgreSQL) - Sequelize

- Link to the ER diagram is [this](https://whimsical.com/er-reddit-dbms-YN6cYTmkMmKxjQe51gJSim).
- Link to SQL Roadmap is [this](https://www.notion.so/SQL-Learning-Roadmap-74f79535b8fd4eb99fe7d47e55305d40).

## Models

- user
- subreddit
- subreddit_member
- post
- post_vote
- comment
- comment_vote
- moderator

## Commands

- npm start
- npm run dev
- npx sequlize-cli init
- create new migration file: `npx sequelize-cli migration:generate --name [file_name]`

- **MIGRATIONS**

  - execute the up function of most recent migration file => `npx sequelize-cli db:migrate`
  - execute the down function of most recent migration file => `npx sequelize-cli db:migrate:undo`
  - execute the down function till the last migration file => `npx sequelize-cli db:migrate:undo:all`
  - execute the down function till the specified migration file => `npx sequelize-cli db:migrate:undo:all --to xxxxxxxxxxxxxx-[file_name].js`

- **SEEDER FILE**

  - generating a seeder file => `npx sequelize-cli seed:generate --name [file_name]`
  - run all seeds => `npx sequelize-cli db:seed:all`
  - run a specific seed => `npx sequelize-cli db:seed --seed [file_name]`
  - undo most recent seed => `npx sequelize-cli db:seed:undo`
  - undo all seeds => `npx sequelize-cli db:seed:undo:all`
  - undo a specific seed => `npx sequelize-cli db:seed:undo --seed [file_name]`

**SQL Query to get top 10 comments of a post**

```sql
select * from comments
where id in (
	select comment_id from comment_votes
	where comment_id in (
		select id from comments
		where post_id = 884
	)
	and voted = 'up'
	group by comment_id
	order by count(voter_id)
) limit 10;
```

## Problem Area

- parent_comment should belong to the same post => currently our seeder is taking any of the comment as parent comment
