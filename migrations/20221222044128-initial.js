'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(sequelize, DataTypes) {
    await sequelize.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      desc: {
        type: DataTypes.STRING,
      },
    });

    await sequelize.createTable('subreddits', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      desc: {
        type: DataTypes.STRING,
      },
      owner_name: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });

    await sequelize.createTable('subreddit_members', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      subreddit_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'subreddits',
          key: 'id',
        },
        allowNull: false,
      },
    });

    await sequelize.createTable('moderators', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      power_given: {
        type: DataTypes.ENUM('topmost', 'mediocre', 'lowermost'),
        allowNull: false,
      },
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      subreddit_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'subreddits',
          key: 'id',
        },
        allowNull: false,
      },
    });

    await sequelize.createTable('posts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      post_heading: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      post_image: {
        type: DataTypes.STRING,
      },
      post_desc: {
        type: DataTypes.STRING,
      },
      post_delete: {
        type: DataTypes.BOOLEAN,
      },
      creator_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      subreddit_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'subreddits',
          key: 'id',
        },
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });

    await sequelize.createTable('post_votes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      voted: {
        type: DataTypes.ENUM('up', 'down'),
        allowNull: false,
      },
      voter_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      post_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'posts',
          key: 'id',
        },
        allowNull: false,
      },
    });

    await sequelize.createTable('comments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      text: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      parent_component: {
        type: DataTypes.INTEGER,
        default: null,
        references: {
          model: 'comments',
          key: 'id',
        },
        allowNull: true,
      },
      creator_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      post_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'posts',
          key: 'id',
        },
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });

    await sequelize.createTable('comment_votes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      voted: {
        type: DataTypes.ENUM('up', 'down'),
        allowNull: false,
      },
      voter_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'users',
          key: 'id',
        },
        allowNull: false,
      },
      comment_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'comments',
          key: 'id',
        },
        allowNull: false,
      },
    });
  },

  async down(sequelize, DataTypes) {
    // await sequelize.dropTable('users');
    // await sequelize.dropTable('subreddits');
    // await sequelize.dropTable('subreddit_members');
    // await sequelize.dropTable('moderators');
    // await sequelize.dropTable('posts');
    // await sequelize.dropTable('post_votes');
    // await sequelize.dropTable('comments');
    // await sequelize.dropTable('comment_votes');
    await sequelize.dropAllTables();
  },
};
