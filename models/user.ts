export const userModel = (sequelize: any, DataTypes: any) => {
  const User = sequelize.define(
    'user',
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      desc: {
        type: DataTypes.STRING,
      },
    },
    { timestamp: false }
  );

  User.associate = (models: any) => {
    User.hasOne(models.subreddit, {
      foreignKey: {
        allowNull: false,
        name: 'owner_name',
      },
    });
    User.hasMany(models.subreddit_member, {
      foreignKey: {
        allowNull: false,
        name: 'user_id',
      },
    });
    User.hasMany(models.moderator, {
      foreignKey: {
        allowNull: false,
        name: 'user_id',
      },
    });
    User.hasMany(models.post, {
      foreignKey: {
        allowNull: false,
        name: 'creator_id',
      },
    });
    User.hasMany(models.comment, {
      foreignKey: {
        allowNull: false,
        name: 'creator_id',
      },
    });
    User.hasMany(models.comment_vote, {
      foreignKey: {
        allowNull: false,
        name: 'voter_id',
      },
    });
    User.hasMany(models.post_vote, {
      foreignKey: {
        allowNull: false,
        name: 'voter_id',
      },
    });
  };

  return User;
};
