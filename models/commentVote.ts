export const commentVoteModel = (sequelize: any, DataTypes: any) => {
  const CommentVote = sequelize.define(
    'comment_vote',
    {
      voted: {
        type: DataTypes.ENUM('up', 'down'),
        allowNull: false,
      },
    },
    { timestamps: false }
  );

  CommentVote.associate = (models: any) => {
    CommentVote.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'voter_id',
      },
    });
    CommentVote.belongsTo(models.comment, {
      foreignKey: {
        allowNull: false,
        name: 'comment_id',
      },
    });
  };

  return CommentVote;
};
