export const subredditModel = (sequelize: any, DataTypes: any) => {
  const Subreddit = sequelize.define(
    'subreddit',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      desc: {
        type: DataTypes.STRING,
      },
    },
    { timestamps: true, createdAt: 'created_at', updatedAt: false }
  );

  Subreddit.associate = (models: any) => {
    Subreddit.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'owner_name',
      },
    });
    Subreddit.hasMany(models.subreddit_member, {
      foreignKey: {
        allowNull: false,
        name: 'subreddit_id',
      },
    });
    Subreddit.hasMany(models.moderator, {
      foreignKey: {
        allowNull: false,
        name: 'subreddit_id',
      },
    });
    Subreddit.hasMany(models.post, {
      foreignKey: {
        allowNull: false,
        name: 'subreddit_id',
      },
    });
  };

  return Subreddit;
};
