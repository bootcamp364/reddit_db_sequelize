export const postModel = (sequelize: any, DataTypes: any) => {
  const Post = sequelize.define(
    'post',
    {
      post_heading: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      post_image: {
        type: DataTypes.STRING,
      },
      post_desc: {
        type: DataTypes.STRING,
      },
      post_delete: {
        type: DataTypes.BOOLEAN,
      },
    },
    { timestamps: true, createdAt: 'created_at', updatedAt: false }
  );

  Post.associate = (models: any) => {
    Post.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'creator_id',
      },
    });
    Post.belongsTo(models.subreddit, {
      foreignKey: {
        allowNull: false,
        name: 'subreddit_id',
      },
    });
    Post.hasMany(models.comment, {
      foreignKey: {
        allowNull: false,
        name: 'post_id',
      },
    });
    Post.hasMany(models.post_vote, {
      foreignKey: {
        allowNull: false,
        name: 'post_id',
      },
    });
  };

  return Post;
};
