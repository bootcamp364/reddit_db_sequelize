export const commentModel = (sequelize: any, DataTypes: any) => {
  const Comment = sequelize.define(
    'comment',
    {
      text: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    { timestamps: true, createdAt: 'created_at', updatedAt: false }
  );

  Comment.associate = (models: any) => {
    Comment.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'creator_id',
      },
    });
    Comment.belongsTo(models.post, {
      foreignKey: {
        allowNull: false,
        name: 'post_id',
      },
    });
    Comment.hasMany(models.comment_vote, {
      foreignKey: {
        allowNull: false,
        name: 'comment_id',
      },
    });
    Comment.hasMany(models.comment, {
      foreignKey: {
        allowNull: true,
        name: 'parent_component',
      },
    });
    Comment.belongsTo(models.comment, {
      foreignKey: {
        allowNull: true,
        name: 'parent_component',
      },
    });
  };

  return Comment;
};
