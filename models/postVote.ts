export const postVoteModel = (sequelize: any, DataTypes: any) => {
  const PostVote = sequelize.define(
    'post_vote',
    {
      voted: {
        type: DataTypes.ENUM('up', 'down'),
        allowNull: false,
      },
    },
    { timestamps: false }
  );

  PostVote.associate = (models: any) => {
    PostVote.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'voter_id',
      },
    });
    PostVote.belongsTo(models.post, {
      foreignKey: {
        allowNull: false,
        name: 'post_id',
      },
    });
  };

  return PostVote;
};
