export const subredditMemberModel = (sequelize: any, DataTypes: any) => {
  const SubredditMember = sequelize.define(
    'subreddit_member',
    {},
    { timestamps: false }
  );

  SubredditMember.associate = (models: any) => {
    SubredditMember.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'user_id',
      },
    });
    SubredditMember.belongsTo(models.subreddit, {
      foreignKey: {
        allowNull: false,
        name: 'subreddit_id',
      },
    });
  };

  return SubredditMember;
};
