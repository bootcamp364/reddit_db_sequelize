export const moderatorModel = (sequelize: any, DataTypes: any) => {
  const Moderator = sequelize.define(
    'moderator',
    {
      power_given: {
        type: DataTypes.ENUM('topmost', 'mediocre', 'lowermost'),
        allowNull: false,
      },
    },
    { timestamps: false }
  );

  Moderator.associate = (models: any) => {
    Moderator.belongsTo(models.user, {
      foreignKey: {
        allowNull: false,
        name: 'user_id',
      },
    });
    Moderator.belongsTo(models.subreddit, {
      foreignKey: {
        allowNull: false,
        name: 'subreddit_id',
      },
    });
  };

  return Moderator;
};
