import models from '../models';
import exp from 'express';

const createUser = async (req: exp.Request, res: exp.Response) => {
  const { username, desc } = req.body;
  const data = { username, desc };
  try {
    const newUser = await models.User.create(data);
    res.status(201).json({ success: true, newUser });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

const getAllUsers = async (req: exp.Request, res: exp.Response) => {
  try {
    const allUsers = await models.User.findAll();
    res.status(201).json({ success: true, allUsers });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

export default {
  createUser,
  getAllUsers,
};
