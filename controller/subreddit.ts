import models from '../models';
import exp from 'express';

const createSubreddit = async (req: exp.Request, res: exp.Response) => {
  const { name, desc, ownerName } = req.body;
  const data = { name, desc, ownerName };
  try {
    const newSubreddit = await models.Subreddit.create(data);
    res.status(201).json({ success: true, newSubreddit });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

const getAllSubreddits = async (req: exp.Request, res: exp.Response) => {
  try {
    const allSubreddits = await models.Subreddit.findAll();
    res.status(201).json({ success: true, allSubreddits });
  } catch (err) {
    res.status(401).json({ success: false, err });
  }
};

export default {
  createSubreddit,
  getAllSubreddits,
};
