import express from 'express';
import db from './models';

import userRouter from './routes/user';
import subredditRouter from './routes/subreddit';

const app = express();
const PORT = process.env.PORT || 4000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/api/user', userRouter);
app.use('/api/subreddit', subredditRouter);

db.sequelize.sync({ force: true }).then(() => {
  app.listen(PORT, () => {
    // console.log(`listening at: http://localhost:${PORT}`);
  });
});
