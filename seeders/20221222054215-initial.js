const { faker } = require('@faker-js/faker');
const ULTRA_MAX_NUMBER = 100000;
const MAX_NUMBER = 50000;
const MED_NUMBER = 5000;

('use strict');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    // USERS
    let users_json = [];
    for (let i = 0; i < MAX_NUMBER; i++) {
      users_json.push({
        username: `${faker.name.firstName()}_${faker.name.lastName()}`,
        desc: faker.lorem.lines(1),
      });
    }
    let users_data_stored = await queryInterface.bulkInsert(
      'users',
      users_json,
      { returning: true }
    );

    // SUBREDDITS
    let subreddits_json = [];
    for (let i = 0; i < MAX_NUMBER; i++) {
      const randomNumber = Math.floor(Math.random() * MAX_NUMBER);
      subreddits_json.push({
        name: faker.name.fullName(),
        desc: faker.lorem.lines(1),
        owner_name: users_data_stored[randomNumber].id,
        created_at: new Date(),
      });
    }
    let subreddits_data_stored = await queryInterface.bulkInsert(
      'subreddits',
      subreddits_json,
      { returning: true }
    );

    // SUBREDDIT_MEMBERS
    let subreddit_members_json = [];
    for (let i = 0; i < MAX_NUMBER; i++) {
      const randomNumber1 = Math.floor(Math.random() * MAX_NUMBER);
      const randomNumber2 = Math.floor(Math.random() * MAX_NUMBER);
      subreddit_members_json.push({
        subreddit_id: subreddits_data_stored[randomNumber1].id,
        user_id: users_data_stored[randomNumber2].id,
      });
    }
    await queryInterface.bulkInsert(
      'subreddit_members',
      subreddit_members_json,
      {}
    );

    // MODERATORS
    let moderators_json = [];
    for (let i = 0; i < MED_NUMBER; i++) {
      const randomNumber1 = Math.floor(Math.random() * MAX_NUMBER);
      const randomNumber2 = Math.floor(Math.random() * MAX_NUMBER);
      moderators_json.push({
        power_given: faker.helpers.arrayElement([
          'topmost',
          'mediocre',
          'lowermost',
        ]),
        user_id: users_data_stored[randomNumber2].id,
        subreddit_id: subreddits_data_stored[randomNumber1].id,
      });
    }
    await queryInterface.bulkInsert('moderators', moderators_json, {});

    // POSTS
    let posts_json = [];
    for (let i = 0; i < ULTRA_MAX_NUMBER; i++) {
      const randomNumber1 = Math.floor(Math.random() * MAX_NUMBER);
      const randomNumber2 = Math.floor(Math.random() * MAX_NUMBER);
      posts_json.push({
        post_heading: faker.lorem.slug(),
        post_image: faker.image.fashion(),
        post_desc: faker.lorem.lines(1),
        post_delete: faker.helpers.arrayElement([true, false]),
        creator_id: users_data_stored[randomNumber2].id,
        subreddit_id: subreddits_data_stored[randomNumber1].id,
        created_at: new Date(),
      });
    }
    let posts_data_stored = await queryInterface.bulkInsert(
      'posts',
      posts_json,
      { returning: true }
    );

    // POST_VOTES
    let post_votes_json = [];
    for (let i = 0; i < ULTRA_MAX_NUMBER; i++) {
      const randomNumber1 = Math.floor(Math.random() * MAX_NUMBER);
      const randomNumber2 = Math.floor(Math.random() * MED_NUMBER);
      post_votes_json.push({
        voted: faker.helpers.arrayElement(['up', 'down']),
        voter_id: users_data_stored[randomNumber1].id,
        post_id: posts_data_stored[randomNumber2].id,
      });
    }
    await queryInterface.bulkInsert('post_votes', post_votes_json, {});

    // COMMENTS
    let comments_json = [];
    for (let i = 0; i < ULTRA_MAX_NUMBER; i++) {
      const randomNumber1 = Math.floor(Math.random() * MAX_NUMBER);
      const randomNumber2 = Math.floor(Math.random() * MED_NUMBER);
      comments_json.push({
        text: faker.lorem.sentence(),
        parent_component: null,
        creator_id: users_data_stored[randomNumber1].id,
        post_id: posts_data_stored[randomNumber2].id,
        created_at: new Date(),
      });
    }
    let comments_data_stored = await queryInterface.bulkInsert(
      'comments',
      comments_json,
      { returning: true }
    );

    // SUB_COMMENTS
    let sub_comments_json = [...comments_data_stored];
    for (let i = 0; i < ULTRA_MAX_NUMBER; i++) {
      const randomNumber_ = Math.floor(Math.random() * MED_NUMBER);
      const commentId_ = comments_data_stored[randomNumber_].id;
      sub_comments_json[i].parent_component = commentId_;
    }
    await queryInterface.bulkDelete('comments', null, {});
    let sub_comments_data_stored = await queryInterface.bulkInsert(
      'comments',
      sub_comments_json,
      { returning: true }
    );

    // COMMENT_VOTES
    let comment_votes_json = [];
    for (let i = 0; i < ULTRA_MAX_NUMBER; i++) {
      const randomNumber1 = Math.floor(Math.random() * MAX_NUMBER);
      const randomNumber2 = Math.floor(Math.random() * MED_NUMBER);
      comment_votes_json.push({
        voted: faker.helpers.arrayElement(['up', 'down']),
        voter_id: users_data_stored[randomNumber1].id,
        comment_id: sub_comments_data_stored[randomNumber2].id,
      });
    }
    await queryInterface.bulkInsert('comment_votes', comment_votes_json, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', null, {});
    await queryInterface.bulkDelete('subreddits', null, {});
    await queryInterface.bulkDelete('subreddit_members', null, {});
    await queryInterface.bulkDelete('moderators', null, {});
    await queryInterface.bulkDelete('posts', null, {});
    await queryInterface.bulkDelete('post_votes', null, {});
    await queryInterface.bulkDelete('comments', null, {});
    await queryInterface.bulkDelete('comment_votes', null, {});
  },
};
