"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postVoteModel = void 0;
var postVoteModel = function (sequelize, DataTypes) {
    var PostVote = sequelize.define('post_vote', {
        voted: {
            type: DataTypes.ENUM('up', 'down'),
            allowNull: false,
        },
    }, { timestamps: false });
    PostVote.associate = function (models) {
        PostVote.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
                name: 'voter_id',
            },
        });
        PostVote.belongsTo(models.post, {
            foreignKey: {
                allowNull: false,
                name: 'post_id',
            },
        });
    };
    return PostVote;
};
exports.postVoteModel = postVoteModel;
//# sourceMappingURL=postVote.js.map