"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userModel = void 0;
var userModel = function (sequelize, DataTypes) {
    var User = sequelize.define('user', {
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        desc: {
            type: DataTypes.STRING,
        },
    }, { timestamp: false });
    User.associate = function (models) {
        User.hasOne(models.subreddit, {
            foreignKey: {
                allowNull: false,
                name: 'owner_name',
            },
        });
        User.hasMany(models.subreddit_member, {
            foreignKey: {
                allowNull: false,
                name: 'user_id',
            },
        });
        User.hasMany(models.moderator, {
            foreignKey: {
                allowNull: false,
                name: 'user_id',
            },
        });
        User.hasMany(models.post, {
            foreignKey: {
                allowNull: false,
                name: 'creator_id',
            },
        });
        User.hasMany(models.comment, {
            foreignKey: {
                allowNull: false,
                name: 'creator_id',
            },
        });
        User.hasMany(models.comment_vote, {
            foreignKey: {
                allowNull: false,
                name: 'voter_id',
            },
        });
        User.hasMany(models.post_vote, {
            foreignKey: {
                allowNull: false,
                name: 'voter_id',
            },
        });
    };
    return User;
};
exports.userModel = userModel;
//# sourceMappingURL=user.js.map