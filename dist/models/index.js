'use strict';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var path_1 = require("path");
var sequelize_1 = require("sequelize");
var process_1 = require("process");
var basename = (0, path_1.basename)(__filename);
var env = process_1.env.NODE_ENV || 'development';
var config_1 = __importDefault(require("../config/config"));
var config = config_1.default[env];
var db = {};
var sequelize;
if (config.use_env_variable) {
    sequelize = new sequelize_1.Sequelize(process_1.env[config.use_env_variable], config);
}
else {
    sequelize = new sequelize_1.Sequelize(config.database, config.username, config.password, config);
}
(0, fs_1.readdirSync)(__dirname)
    .filter(function (file) {
    return (file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js');
})
    .forEach(function (file) {
    var model = require((0, path_1.join)(__dirname, file))(sequelize, sequelize_1.DataTypes);
    db[model.name] = model;
});
Object.keys(db).forEach(function (modelName) {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});
db.sequelize = sequelize;
db.Sequelize = sequelize_1.Sequelize;
exports.default = db;
//# sourceMappingURL=index.js.map