"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.commentVoteModel = void 0;
var commentVoteModel = function (sequelize, DataTypes) {
    var CommentVote = sequelize.define('comment_vote', {
        voted: {
            type: DataTypes.ENUM('up', 'down'),
            allowNull: false,
        },
    }, { timestamps: false });
    CommentVote.associate = function (models) {
        CommentVote.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
                name: 'voter_id',
            },
        });
        CommentVote.belongsTo(models.comment, {
            foreignKey: {
                allowNull: false,
                name: 'comment_id',
            },
        });
    };
    return CommentVote;
};
exports.commentVoteModel = commentVoteModel;
//# sourceMappingURL=commentVote.js.map