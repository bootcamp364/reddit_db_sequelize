"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postModel = void 0;
var postModel = function (sequelize, DataTypes) {
    var Post = sequelize.define('post', {
        post_heading: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        post_image: {
            type: DataTypes.STRING,
        },
        post_desc: {
            type: DataTypes.STRING,
        },
        post_delete: {
            type: DataTypes.BOOLEAN,
        },
    }, { timestamps: true, createdAt: 'created_at', updatedAt: false });
    Post.associate = function (models) {
        Post.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
                name: 'creator_id',
            },
        });
        Post.belongsTo(models.subreddit, {
            foreignKey: {
                allowNull: false,
                name: 'subreddit_id',
            },
        });
        Post.hasMany(models.comment, {
            foreignKey: {
                allowNull: false,
                name: 'post_id',
            },
        });
        Post.hasMany(models.post_vote, {
            foreignKey: {
                allowNull: false,
                name: 'post_id',
            },
        });
    };
    return Post;
};
exports.postModel = postModel;
//# sourceMappingURL=post.js.map