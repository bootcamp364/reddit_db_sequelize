"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.subredditMemberModel = void 0;
var subredditMemberModel = function (sequelize, DataTypes) {
    var SubredditMember = sequelize.define('subreddit_member', {}, { timestamps: false });
    SubredditMember.associate = function (models) {
        SubredditMember.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
                name: 'user_id',
            },
        });
        SubredditMember.belongsTo(models.subreddit, {
            foreignKey: {
                allowNull: false,
                name: 'subreddit_id',
            },
        });
    };
    return SubredditMember;
};
exports.subredditMemberModel = subredditMemberModel;
//# sourceMappingURL=subredditMember.js.map