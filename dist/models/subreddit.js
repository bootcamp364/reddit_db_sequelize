"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.subredditModel = void 0;
var subredditModel = function (sequelize, DataTypes) {
    var Subreddit = sequelize.define('subreddit', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        desc: {
            type: DataTypes.STRING,
        },
    }, { timestamps: true, createdAt: 'created_at', updatedAt: false });
    Subreddit.associate = function (models) {
        Subreddit.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
                name: 'owner_name',
            },
        });
        Subreddit.hasMany(models.subreddit_member, {
            foreignKey: {
                allowNull: false,
                name: 'subreddit_id',
            },
        });
        Subreddit.hasMany(models.moderator, {
            foreignKey: {
                allowNull: false,
                name: 'subreddit_id',
            },
        });
        Subreddit.hasMany(models.post, {
            foreignKey: {
                allowNull: false,
                name: 'subreddit_id',
            },
        });
    };
    return Subreddit;
};
exports.subredditModel = subredditModel;
//# sourceMappingURL=subreddit.js.map