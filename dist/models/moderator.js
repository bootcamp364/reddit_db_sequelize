"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.moderatorModel = void 0;
var moderatorModel = function (sequelize, DataTypes) {
    var Moderator = sequelize.define('moderator', {
        power_given: {
            type: DataTypes.ENUM('topmost', 'mediocre', 'lowermost'),
            allowNull: false,
        },
    }, { timestamps: false });
    Moderator.associate = function (models) {
        Moderator.belongsTo(models.user, {
            foreignKey: {
                allowNull: false,
                name: 'user_id',
            },
        });
        Moderator.belongsTo(models.subreddit, {
            foreignKey: {
                allowNull: false,
                name: 'subreddit_id',
            },
        });
    };
    return Moderator;
};
exports.moderatorModel = moderatorModel;
//# sourceMappingURL=moderator.js.map