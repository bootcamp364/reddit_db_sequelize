"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var models_1 = __importDefault(require("./models"));
var user_1 = __importDefault(require("./routes/user"));
var subreddit_1 = __importDefault(require("./routes/subreddit"));
var app = (0, express_1.default)();
var PORT = process.env.PORT || 4000;
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
app.use('/api/user', user_1.default);
app.use('/api/subreddit', subreddit_1.default);
models_1.default.sequelize.sync({ force: true }).then(function () {
    app.listen(PORT, function () {
        // console.log(`listening at: http://localhost:${PORT}`);
    });
});
//# sourceMappingURL=server.js.map