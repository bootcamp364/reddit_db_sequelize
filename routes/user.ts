import { Router } from 'express';
import userController from '../controller/user';

const router = Router();

router.post('/', userController.createUser);
router.get('/', userController.getAllUsers);

export default router;
