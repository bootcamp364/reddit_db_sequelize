import { Router } from 'express';
import subredditController from '../controller/subreddit';

const router = Router();

router.post('/', subredditController.createSubreddit);
router.get('/', subredditController.getAllSubreddits);

export default router;
