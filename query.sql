select * from comment_votes;

select * from comments;

-- get comments with number of upvotes => comment_id: 1181
select comment_id, count(voted) as number_of_upvotes
from comment_votes
where voted = 'up'
group by comment_id
order by number_of_upvotes desc;

-- find which post has the comment with max upvotes => post_id: 884
select * from comments
where id = 1181;

-- get all the commentIds of this particular post
select id from comments
where post_id = 884;

-- get count of comment votes of all comments of the particular post
select comment_id, count(voter_id) as number_of_upvotes from comment_votes
where comment_id in (
	select id from comments
	where post_id = 884
) 
and voted = 'up'
group by comment_id
order by number_of_upvotes;

-- get top comment_ids of a post 
select comment_id from comment_votes
where comment_id in (
	select id from comments
	where post_id = 884
) 
and voted = 'up'
group by comment_id
order by count(voter_id);

-- get top comments of a post 
select * from comments
where id in (
	select comment_id from comment_votes
	where comment_id in (
		select id from comments
		where post_id = 884
	) 
	and voted = 'up'
	group by comment_id
	order by count(voter_id)
);